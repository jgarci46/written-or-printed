from pickle import FALSE
import cv2
import numpy as np
import os
import random
import importModel
import shutil

def center_crop(image,out_height,out_width):
  input_height, input_width = image.shape[:2]

  offset_height = (input_height - out_height) // 2
  offset_width = (input_width - out_width) // 2

  image = image[offset_height:offset_height+out_height, offset_width:offset_width+out_width]

  return image


def resize_maintain_aspect(image, target_h, target_w):
  input_height, input_width = image.shape[:2]
  if input_height > input_width:
    new_width = target_w
    new_height = int(input_height*(target_w/input_width))
  else:
    new_height = target_h
    new_width = int(input_width*(target_h/input_height))

  image = cv2.resize(image,(new_width,new_height),interpolation=cv2.INTER_CUBIC)
  return image

def modelRunner(filename, directory):
  dir=directory
  label_file=filename
  output_file="dataset"
  input_height=640
  input_width=480
  input_colors=1
  sizePerFile=1000
  normalize=False

  file = open(label_file,  'r')
  ImageNames= file.readlines()
  file.close()
  counter = 0
  labels=np.array([], dtype=np.int64).reshape(0,2)




  while((len(ImageNames)-(counter*sizePerFile))>sizePerFile):
    y= np.ndarray((sizePerFile, 2), dtype=np.uint8, order='C')


    X= np.ndarray((sizePerFile, (input_height * input_width)), dtype=np.float32, order='c')

    for i in range(sizePerFile):



     


      imagename = ImageNames[i+counter*sizePerFile]

      curFile = dir + "/" + imagename.strip()
      print(curFile)
      img = cv2.imread(curFile, 0)



      img = resize_maintain_aspect(img, input_height, input_width)

      img = center_crop(img, input_height, input_width)
  
      img = img.flatten()
      #img = np.resize(img,(input_height*input_width))
      img.resize(input_height*input_width, refcheck=False)
      if normalize:
          X[i] = (img / 255.0)
      else:
          X[i] = img



    counter+=1

    print('x shape ', X.shape)
    labels.append(importModel.model(X))






  X= np.ndarray((len(ImageNames)-(counter*sizePerFile), (input_height * input_width)), dtype=np.float32, order='c')

  for i in range(len(ImageNames)-counter*sizePerFile):



      imagename = ImageNames[i+counter*sizePerFile]

      curFile = dir + "/" + imagename.strip()
      print(curFile)
      img = cv2.imread(curFile, 0)



      img = resize_maintain_aspect(img, input_height, input_width)

      img = center_crop(img, input_height, input_width)
  
      img = img.flatten()
      img.resize(input_height*input_width, refcheck=False)
      #img = np.resize(img,(input_height*input_width))
      if normalize:
          X[i] = (img / 255.0)
      else:
          X[i] = img
  labels= np.append(labels, importModel.model(X), axis=0)
  print(labels)

  for i in range(len(ImageNames)):
      imagename = ImageNames[i]
      curFile = dir + "/" + imagename.strip()
      #print(labels[0])
      #print(labels[i][0])
      #print(labels[i][1])
     # print(labels[i][0][0])
     # print(labels[i][0][1])
      if(labels[i][0] == 1):
        dest = "./Written" + "/" + imagename.strip()
        os.rename(curFile, dest)
        #os.replace(curFile, dest)
        #shutil.move(curFile, dest)
      elif(labels[i][1] == 1):
        dest = "./Printed"  + "/" + imagename.strip()
        os.rename(curFile, dest)
        #os.replace(curFile, dest)
        #shutil.move(curFile, dest)
      else:
        dest = "./Unknown"  + "/" + imagename.strip()
        os.rename(curFile, dest)
        #os.replace(curFile, dest)
        #shutil.move(curFile, dest)

  return labels





