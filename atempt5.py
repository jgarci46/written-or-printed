import cv2
import numpy as np
import os
import random

def center_crop(image,out_height,out_width):
  input_height, input_width = image.shape[:2]

  offset_height = (input_height - out_height) // 2
  offset_width = (input_width - out_width) // 2

  image = image[offset_height:offset_height+out_height, offset_width:offset_width+out_width]

  return image


def resize_maintain_aspect(image, target_h, target_w):
  input_height, input_width = image.shape[:2]
  if input_height > input_width:
    new_width = target_w
    new_height = int(input_height*(target_w/input_width))
  else:
    new_height = target_h
    new_width = int(input_width*(target_h/input_height))

  image = cv2.resize(image,(new_width,new_height),interpolation=cv2.INTER_CUBIC)
  return image

dir=".untared/images"
label_file=".untared/labels/test.txt"
output_file="dataset"
input_height=640
input_width=480
input_colors=1
sizePerFile=1000
normalize=False

file = open(label_file,  'r')
ImageNames= file.readlines()
file.close()
counter = 0
for i in range(len(ImageNames)-1,1,-1):
  imagename, label = ImageNames[i].split()

  label = int(label)
  if (label==13 or label==15 or label==4 or label==0 or label==12 or label==8 or label==10 or label==5 or label ==11 or label==2 or label==1 or label ==7):
    ImageNames.pop(i)



while((len(ImageNames)-(counter*sizePerFile))>sizePerFile):
  y= np.ndarray((sizePerFile, 2), dtype=np.uint8, order='C')


  X= np.ndarray((sizePerFile, (input_height * input_width)), dtype=np.float32, order='c')

  for i in range(sizePerFile):



    imagename, label = ImageNames[i+counter*sizePerFile].split()


    img = cv2.imread(os.path.join(dir, imagename), 0)



    img = resize_maintain_aspect(img, input_height, input_width)

    img = center_crop(img, input_height, input_width)
 
    img = img.flatten()
    img.resize(input_height*input_width)
    if normalize:
        X[i] = (img / 255.0)
    else:
        X[i] = img
    label = int(label)
    if (label == 3):
        yValue = [1, 0]
    elif (label != 3):
        yValue = [0, 1]
    else:
        yValue = [0, 0]
   

    y[i] = yValue
  counter+=1

  print('x shape ', X.shape)
  print('y shape ', y.shape)
  np.savez(output_file+str(counter), X=X, y=y)
  print(str((len(ImageNames)-(counter*sizePerFile)))+"and the counter number is"+str(counter)+"  while image names is" + str(len(ImageNames)) )


y= np.ndarray((len(ImageNames)-(counter*sizePerFile), 3), dtype=np.uint8, order='C')


X= np.ndarray((len(ImageNames)-(counter*sizePerFile), (input_height * input_width)), dtype=np.float32, order='c')

for i in range(len(ImageNames)-counter*sizePerFile):



    imagename, label = ImageNames[i+counter*sizePerFile].split()


    img = cv2.imread(os.path.join(dir, imagename), 0)



    img = resize_maintain_aspect(img, input_height, input_width)

    img = center_crop(img, input_height, input_width)
 
    img = img.flatten()
    img.resize(input_height*input_width)
    if normalize:
        X[i] = (img / 255.0)
    else:
        X[i] = img
    label = int(label)
    if (label == 3):
        yValue = [1, 0, 0]
    elif (label != 3):
        yValue = [0, 1, 0]
    else:
        yValue = [0, 0, 1]
   

    y[i] = yValue

print('x shape ', X.shape)
print('y shape ', y.shape)
np.savez(output_file+str(counter), X=X, y=y)
