import tkinter as tk
from tkinter import *
from tkinter import NW
from tkinter.filedialog import askopenfilename
from PIL import ImageTk, Image
from PIL.ImageTk import PhotoImage


def open_file():
    """Open a file for editing."""
    filepath = askopenfilename(
        filetypes=[("PNG Files", "*.png"), ("All Files", "*.*")]
    )
    if not filepath:
        return
    txt_edit.delete("1.0", tk.END)
    with open(filepath, mode="r", encoding="utf-8") as input_file:
        text = input_file.read()
        txt_edit.insert(tk.END, text)
    window.title(f"Written or Printed - {filepath}")



window = tk.Tk()
window.title("Written or Printed")

# Derrick - changed the window sizes
window.rowconfigure(1, minsize=10, weight=1)
window.columnconfigure(0, minsize=10, weight=1)




# START Derrick's work
# adds icon on top left of window
window.iconbitmap('images/LogoForClass.ico')

# resizing window to be smaller, so it doesn't look stupidly big
window.geometry("300x205")
# window.geometry("1000x700") this is for the 1000 by 700 pic

# trying to make background image, defining the bg
bg = Image.open("images/FinalPicture.png")
bg = ImageTk.PhotoImage(bg)
my_bg = tk.Label(image=bg)
my_bg.place(x=0, y=0, relwidth=1, relheight=1)


# making a background canvas
'''my_canvas = tk.Canvas(width=1000, height= 700)
my_canvas.image = bg'''
# setting the image to canvas
# first create the canvas, then set an image to the canvas


# adding logo on top of background
#image = PhotoImage(file="images/IMG_0656.PNG")
#window.create_image(0, 0, anchor = NW, image = image)

'''
my_img = Image.open("images/LogoNoWhiteSpacePNG.PNG")
my_img = ImageTk.PhotoImage(my_img)
my_img_label = tk.Label(image = my_img)
my_img_label.image = my_img
my_img_label.grid(column=0, row=1)'''

# txt_edit = tk.Text(window)
# frm_buttons = tk.Frame(window, relief=tk.RAISED, bd=2)
btn_open = tk.Button(window, text="Open", command=open_file)


# Derrick - changed the button sizes
btn_open.grid(row=80, column=0, sticky="ns", padx=0, pady=0)

# Derrick - changed the button sizes, also from buttons suck
# frm_buttons.grid(row=90, column=0, sticky="ns")

# this is for the text editor, we will most likely not need this
# txt_edit.grid(row=0, column=0, sticky="nsew")

#window.pack()

# end of Derrick's work


window.mainloop()