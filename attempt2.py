import tkinter as tk
from tkinter import NW
from tkinter.filedialog import askopenfilename
from PIL import ImageTk, Image
from PIL.ImageTk import PhotoImage


def open_file():
    """Open a file for editing."""
    filepath = askopenfilename(
        filetypes=[("PNG Files", "*.png"), ("All Files", "*.*")]
    )
    if not filepath:
        return
    txt_edit.delete("1.0", tk.END)
    with open(filepath, mode="r", encoding="utf-8") as input_file:
        text = input_file.read()
        txt_edit.insert(tk.END, text)
    window.title(f"Written or Printed - {filepath}")



window = tk.Tk()
window.title("Written or Printed")

window.rowconfigure(1, minsize=100, weight=1)
window.columnconfigure(0, minsize=100, weight=1)

txt_edit = tk.Text(window)
frm_buttons = tk.Frame(window, relief=tk.RAISED, bd=2)
btn_open = tk.Button(frm_buttons, text="Open", command=open_file)


btn_open.grid(row=0, column=0, sticky="ew", padx=100, pady=50)


frm_buttons.grid(row=90, column=0, sticky="ns")
txt_edit.grid(row=1, column=0, sticky="nsew")


# START Derrick's work

# adds icon on top left
window.iconbitmap('images/LogoForClass.ico')

# image on sides
# my_img = ImageTk.PhotoImage(Image.open("images/IMG_0656.PNG"))
my_img = PhotoImage(file="images/IMG_0656.PNG")
# window.ima
# end of Derrick's work


window.mainloop()