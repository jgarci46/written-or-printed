from tkinter import *
from tkinter import NW
from tkinter.filedialog import *
from tkinter import filedialog
from PIL import ImageTk, Image
import os
import time


window = Tk()
window.title("Written or Printed")
folder_path = StringVar()

window.iconbitmap('images/LogoForClass.ico')
window.geometry("300x205")
window.resizable(False, False)

def open_file():
    """Open a file for editing."""
    global folder_path 
    filepath = askdirectory()
    folder_path.set(filepath)
    print(filepath) 
    dir_list = os.listdir(filepath)
    print(dir_list)

   # files = os.listdir(filepath)
    #files = [f for f in files if os.path.isfile(filepath+'/'+f)]
   # master=None
  #  filepath = askdirectory(title="Select folder", initialdir=".", parent=None if master is None else master.tk)
      #  filetypes=[("PNG Files", "*.png"), ("All Files", "*.*")]
    #)
    '''
    if not filepath:
        return
    txt_edit.delete("1.0", tk.END)
    with open(filepath, mode="r", encoding="utf-8") as input_file:
        text = input_file.read()
        txt_edit.insert(tk.END, text)
    window.title(f"Written or Printed - {filepath}")
'''
def open_Write():
    """Open the written folder"""
    path =  "./Written"
    path = os.path.realpath(path)
    os.startfile(path)
def open_Print():
    """Open the printed folder"""
    path = "./Printed"
    path = os.path.realpath(path)
    os.startfile(path)
def open_IDK():
    """Open the unknown folder"""
    path = "./Unknown"
    path = os.path.realpath(path)
    os.startfile(path)

#instead of open_file command. Change it to a command to open the newly created "written" folder
WrittenButton = Button(window, text="Written", command=open_Write)
WrittenButton.pack(pady=15)

PrintedButton = Button(window, text="Printed", command=open_Print)
PrintedButton.pack(pady=25)

IDKButton = Button(window, text="Unknown", command=open_IDK)
IDKButton.pack(pady=25)

'''
https://www.youtube.com/watch?v=0WRMYdOwHYE&ab_channel=BroCode
Progress bar. Need to remake based on the algorithm  
def start():
    GB = 100
    download = 0
    speed = 1
    while(download<GB):
        time.sleep(0.05)
        bar['value']+=(speed/GB)*100
        download+=speed
        percent.set(str(int((download/GB)*100))+"%")
        text.set(str(download)+"/"+str(GB)+" GB completed")
        window.update_idletasks()
        
window = Tk()

percent = StringVar()
text = StringVar()

bar = Progressbar(window,orient=HORIZONTAL,length=300)
bar.pack(pady=10)

percentLabel = Label(window,textvariable=percent).pack()
taskLabel = Label(window,textvariable=text).pack()

button = Button(window,text="download",command=start).pack()
'''

window.mainloop()