import tensorflow as tf
def model(X):
    saved_model = tf.keras.models.load_model("Model2.h5")
    labels = (saved_model.predict(X) > 0.55).astype(int)
    return labels


