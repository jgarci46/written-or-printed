
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import tensorflow as tf

"""
# load the dataset
dataset = np.load("dataset.npz")
# split into input (X) and output (y) variables
X = dataset['X']
y = dataset['y'] 
"""

dataset = np.load("dataset1.npz")
# split into input (X) and output (y) variables
X = dataset['X']

# define the keras model
model = Sequential()
model.add(Dense(400, input_shape=(X.shape[1],), activation='tanh'))
model.add(Dense(200, activation='tanh'))
model.add(Dense(50, activation='tanh'))
model.add(Dense(2, activation='softmax'))
# compile the keras model
#model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.compile(
    loss='mse',
    optimizer=tf.keras.optimizers.Adam(),#learning_rate=0.002
    metrics=['accuracy'])
dataset2 = np.load("dataset7.npz")
X_val = dataset2['X']
y_val = dataset2['y']

for i in range(1,2):
	# load the dataset
	dataset = np.load("dataset"+str(i)+".npz")
	# split into input (X) and output (y) variables
	X = dataset['X']
	y = dataset['y']
	# fit the keras model on the dataset
	model.fit(X, y, epochs=1500, batch_size=2, validation_data=(X_val, y_val))
	# make class predictions with the model   
	predictions = (model.predict(X[:10]))
	# summarize the first 5 cases
	for i in range(3):
		print(' %s (expected %s)' % (predictions[i].tolist(), y[i].tolist()))
	tf.keras.backend.clear_session()